package guiTresEnRatlla;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.io.File;
import java.util.ArrayList;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.border.LineBorder;

import com.sun.glass.events.KeyEvent;

import cat.inspedralbes.PolMarBadMar.GestorFitxers;
import cat.inspedralbes.PolMarBadMar.Jugades;
import cat.inspedralbes.PolMarBadMar.Jugador;
import cat.inspedralbes.PolMarBadMar.Partida;
import java.awt.CardLayout;

public class InterficieGrafica extends JFrame implements ActionListener {

	private JPanel contentPane;
	private Jugador j1 = new Jugador(1, "X");
	private Jugador j2 = new Jugador(5, "O");
	private Partida partida = new Partida();
	private JButton[][] arrayBotons = new JButton[3][3];
	private JPanel tablero;
	private JLabel tornJugador1;
	private JPanel panellCentral;
	private CardLayout cl;
	File fitxer = new File("Partida.bi");
	GestorFitxers gf = new GestorFitxers(fitxer);
	public ArrayList<Jugades> ordre = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterficieGrafica frame = new InterficieGrafica();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterficieGrafica() {

		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 490, 328);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		panellCentral = new JPanel();
		panellCentral.setOpaque(false);
		panellCentral.setRequestFocusEnabled(false);
		contentPane.add(panellCentral);
		cl = new CardLayout();
		panellCentral.setLayout(cl);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnJoc = new JMenu("Joc");
		mnJoc.setMnemonic('J');
		mnJoc.setMnemonic(KeyEvent.VK_J);
		menuBar.add(mnJoc);

		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mntmGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gf.guardarPartida(ordre);
			}
		});
		mntmGuardar.setMnemonic('J');
		mntmGuardar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
		mnJoc.add(mntmGuardar);

		JMenuItem mntmCarregar = new JMenuItem("Carregar");
		mntmCarregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Jugades> carrega = gf.llegirPartida();
				for (int i = 0; i < carrega.size(); i++) {
					Jugades j = carrega.get(i);
					System.out.println("Torn" + (i + 1) + ":" + j.getX() + "," + j.getY());
					mostrarButons();
				}
			}
		});
		mntmCarregar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
		mnJoc.add(mntmCarregar);

		JMenuItem mntmReset = new JMenuItem("Reiniciar");
		mntmReset.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		mntmReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reset();
			}
		});
		mnJoc.add(mntmReset);

		tablero = new JPanel();
		tablero.setMaximumSize(new Dimension(50, 50));
		tablero.setMinimumSize(new Dimension(100, 100));
		tablero.setPreferredSize(new Dimension(350, 190));
		panellCentral.add(tablero, "tablero");
		tablero.setLayout(new GridLayout(3, 3, 10, 10));

		JPanel finalPartida = new JPanel();
		FlowLayout fl_finalPartida = (FlowLayout) finalPartida.getLayout();
		panellCentral.add(finalPartida, "final");

		JLabel msgFinal = new JLabel("FI DE LA PARTIDA");
		msgFinal.setFont(new Font("DejaVu Math TeX Gyre", Font.BOLD, 24));
		finalPartida.add(msgFinal);

		JPanel superior = new JPanel();
		contentPane.add(superior, BorderLayout.NORTH);

		tornJugador1 = new JLabel("Jugador 1 Fitxa ");
		tornJugador1.setVisible(true);
		superior.add(tornJugador1);

		for (int i = 0; i < arrayBotons.length; i++) {
			for (int j = 0; j < arrayBotons.length; j++) {
				arrayBotons[i][j] = new JButton("-");
				tablero.add(arrayBotons[i][j]);
				arrayBotons[i][j].setActionCommand(i + "," + j);
				arrayBotons[i][j].setBorderPainted(false);
				arrayBotons[i][j].setBackground(Color.ORANGE);
				arrayBotons[i][j].setFont(new Font("Dialog", Font.BOLD, 42));
				arrayBotons[i][j].addActionListener(this);
				arrayBotons[i][j].setForeground(Color.BLACK);
			}
		}

		cl.show(panellCentral, "tablero");

	}

	public void reset() {
		partida.reset();
		for (int i = 0; i < arrayBotons.length; i++) {
			for (int j = 0; j < arrayBotons.length; j++) {
				arrayBotons[i][j].setText("-");
				arrayBotons[i][j].removeActionListener(this);
				arrayBotons[i][j].addActionListener(this);
				arrayBotons[i][j].setForeground(Color.BLACK);
			}
		}
		cl.show(panellCentral, "tablero");
		setTornJugadorText("Jugador 1 Fitxa " + j1.getFitxaGUI());
		tornJugador1.setVisible(true);
	}
	
	protected void mostrarButons() {
		reset();
		ArrayList<Jugades> a =gf.llegirPartida();
		for (int i = 0; i < a.size(); i++) {
			Jugades j=a.get(i);
			if (i%2==0) {
				colocarFitxaAlTauler(j.getX(), j.getY(), arrayBotons[j.getX()][j.getY()], j1);
			}else {
				colocarFitxaAlTauler(j.getX(), j.getY(), arrayBotons[j.getX()][j.getY()], j2);
			}
		}		
			
				
			
		
	}

	public void cambiarTornLabel() {
		if (partida.getTorn()) {
			setTornJugadorText("Jugador 1 Fitxa " + j1.getFitxaGUI());
		} else {
			setTornJugadorText("Jugador 2 Fitxa " + j2.getFitxaGUI());
		}
		if (partida.getTauler().gameOver(j1.getFitxa(), j2.getFitxa()) == 1) {
			setTornJugadorText("Victoria del Jugador 1 ");
		} else {
			if (partida.getTauler().gameOver(j1.getFitxa(), j2.getFitxa()) == 2) {
				setTornJugadorText("Victoria del Jugador 2 ");
			} else {
				if (partida.getTauler().gameOver(j1.getFitxa(), j2.getFitxa()) == 3) {
					setTornJugadorText("Empat");
				}
			}
		}
	}

	public void jugada(int x, int y, JButton botoClicat) {
		if (partida.getTorn()) {
			colocarFitxaAlTauler(x, y, botoClicat, j1);
			ordre.add(new Jugades(x, y));
		} else {
			colocarFitxaAlTauler(x, y, botoClicat, j2);
			ordre.add(new Jugades(x, y));
		}
		botoClicat.removeActionListener(this);
		if (partida.getTauler().gameOver(j1.getFitxa(), j2.getFitxa()) != 0) {
			cl.show(panellCentral, "final");
		}
	}

	public String getTornJugadorText() {
		return tornJugador1.getText();
	}

	public void setTornJugadorText(String text) {
		tornJugador1.setText(text);
	}

	private void colocarFitxaAlTauler(int x, int y, JButton botoClicat, Jugador j) {
		partida.juga(x, y, j.getFitxa());
		botoClicat.setText(j.getFitxaGUI());

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String pos = arg0.getActionCommand();
		char xChar;
		char yChar;
		int x;
		int y;

		xChar = pos.charAt(0);
		yChar = pos.charAt(2);
		x = Character.getNumericValue(xChar);
		y = Character.getNumericValue(yChar);
		jugada(x, y, (JButton) arg0.getSource());
		cambiarTornLabel();
	}

}
