package cat.inspedralbes.PolMarBadMar;

import java.io.File;

public class Partida {

	private boolean torn = true;
	
	private int nombreJugades=0;

	private Tauler tauler = new Tauler();

	public void pasarTorn() {
		torn = !torn;
		nombreJugades++;
	}

	public boolean getTorn() {
		return torn;
	}

	public Tauler getTauler() {
		return tauler;
	}

	public void setTauler(Tauler tauler) {
		this.tauler = tauler;
	}

	public void setTorn(boolean torn) {
		this.torn = torn;
	}

	public void juga(int x, int y, int fitxa) {
		if (tauler.comprovarJugada(x, y)) {
			tauler.colocarFitxa(x, y, fitxa);
			pasarTorn();
		}
	}

	public void reset() {
		tauler.inicialitzarTauler();
		torn = true;
	}

}
