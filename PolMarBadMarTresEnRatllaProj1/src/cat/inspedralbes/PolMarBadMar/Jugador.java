package cat.inspedralbes.PolMarBadMar;

public class Jugador {

	private int fitxa;

	private String fitxaGUI;
	
	private int IdJugador;

	public Jugador(int fitxa, String fitxaGUI) {
		this.fitxa = fitxa;
		this.fitxaGUI = fitxaGUI;
	}

	public int getFitxa() {
		return fitxa;
	}

	public void setFitxa(int fitxa) {
		this.fitxa = fitxa;
	}

	public String getFitxaGUI() {
		return fitxaGUI;
	}

	public void setFitxaGUI(String fitxaGUI) {
		this.fitxaGUI = fitxaGUI;
	}
	
	public int getIdJugador() {
		return IdJugador;
	}

	public void setIdJugador(int numJugador) {
		this.IdJugador = numJugador;
	}
}
