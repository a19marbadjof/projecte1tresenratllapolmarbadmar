package cat.inspedralbes.PolMarBadMar;

import java.io.Serializable;

public class Jugades implements Serializable {

	int x, y;

	public Jugades(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
