package cat.inspedralbes.PolMarBadMar;

public class Tauler {

	private int[][] caselles = new int[3][3];

	public int[][] getCaselles() {
		return caselles;
	}

	public void setCaselles(int[][] caselles) {
		this.caselles = caselles;
	}

	public void inicialitzarTauler() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				caselles[i][j] = 0;
			}
		}
	}

	public boolean comprovarJugada(int x, int y) {
		boolean valida = false;
		if (caselles[x][y] == 0) {
			valida = true;
		} else {
			valida = false;
		}
		return valida;
	}

	public void colocarFitxa(int x, int y, int fitxa) {
		caselles[x][y] = fitxa;
	}

	private int comprovarFiles(int fitxa1, int fitxa2) {
		int guanyador = 0;
		int comprovacio = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				comprovacio += caselles[i][j];
			}
			if ((comprovacio == fitxa1 * 3) || (comprovacio == fitxa2 * 3)) {
				if (comprovacio == fitxa1 * 3) {
					guanyador = 1;
				}
				if (comprovacio == fitxa2 * 3) {
					guanyador = 2;
				}
			}
			comprovacio = 0;
		}
		return guanyador;

	}

	private int comprovarColumnes(int fitxa1, int fitxa2) {
		int guanyador = 0;
		int comprovacio = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				comprovacio += caselles[j][i];
			}
			if ((comprovacio == fitxa1 * 3) || (comprovacio == fitxa2 * 3)) {
				if (comprovacio == fitxa1 * 3) {
					guanyador = 1;
				}
				if (comprovacio == fitxa2 * 3) {
					guanyador = 2;
				}

			}
			comprovacio = 0;
		}
		return guanyador;
	}

	private int comprovarDiagonals(int fitxa1, int fitxa2) {
		int guanyador = 0;
		int comprovacio = 0;
		for (int i = 0; i < 3; i++) {
			comprovacio += caselles[i][i];
		}
		if ((comprovacio == fitxa1 * 3) || (comprovacio == fitxa2 * 3)) {
			if (comprovacio == fitxa1 * 3) {
				guanyador = 1;
			}
			if (comprovacio == fitxa2 * 3) {
				guanyador = 2;
			}
		}
		comprovacio = 0;
		for (int i = 0; i < 3; i++) {
			comprovacio += caselles[i][2 - i];
		}
		if ((comprovacio == fitxa1 * 3) || (comprovacio == fitxa2 * 3)) {
			if (comprovacio == fitxa1 * 3) {
				guanyador = 1;
			}
			if (comprovacio == fitxa2 * 3) {
				guanyador = 2;
			}
		}
		return guanyador;
	}

	private int comprovarEmpat() {
		int guanyador = 0;
		int cont = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if ((caselles[i][j] != 0) && (guanyador == 0)) {
					cont++;
				}
			}
		}
		if (cont == 9) {
			guanyador = 3;
		}
		return guanyador;
	}

	public int gameOver(int fitxa1, int fitxa2) {
		int guanyador = 0;
		int cont = 0;
		// Files 1-3
		if (comprovarFiles(fitxa1, fitxa2) != 0) {
			guanyador = comprovarFiles(fitxa1, fitxa2);
		}
		// Columns 1-3
		if (comprovarColumnes(fitxa1, fitxa2) != 0) {
			guanyador = comprovarColumnes(fitxa1, fitxa2);
		}
		// Diagonals
		if (comprovarDiagonals(fitxa1, fitxa2) != 0) {
			guanyador = comprovarDiagonals(fitxa1, fitxa2);
		}
		// Empat
		if (comprovarEmpat() != 0) {
			guanyador = comprovarEmpat();
		}
		return guanyador;
	}
}
