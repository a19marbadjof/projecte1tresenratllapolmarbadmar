package cat.inspedralbes.PolMarBadMar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class GestorFitxers {
	
	File file;

	public GestorFitxers(File file) {
		this.file = file;
	}
	
	public void guardarPartida(ArrayList<Jugades> jugades) {
		try {
		FileOutputStream fo = new FileOutputStream(file);
		ObjectOutputStream oos= new ObjectOutputStream(fo);
		oos.writeObject(jugades);
		oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public ArrayList<Jugades> llegirPartida(){
		
		try {
			FileInputStream fi = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fi);
			ArrayList<Jugades> jugades = (ArrayList<Jugades>) ois.readObject();
			ois.close();
			return jugades;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

}
